# frozen_string_literal: true

module TanukiEmoji
  module Db
    EmojiData = Struct.new(:codepoints, :property, :version, :range_size, :examples, :description)
  end
end
