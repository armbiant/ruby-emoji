# frozen_string_literal: true

require 'json'

module TanukiEmoji
  module Db
    # Emoji Unicode Version database
    class UnicodeVersion
      DATA_FILE = 'vendor/emoji-unicode-version/emoji-unicode-version-map.json'

      def self.data_file
        File.expand_path(File.join(__dir__, '../../../', DATA_FILE))
      end

      attr_reader :data_file

      def initialize(index:, data_file: self.class.data_file)
        @data_file = data_file
        @index = index
      end

      def load!
        db = File.open(data_file, 'r:UTF-8') do |file|
          JSON.parse(file.read, symbolize_names: true)
        end

        db.each do |emoji_name, unicode_version|
          emoji = @index.find_by_alpha_code(emoji_name)

          next unless emoji

          emoji.unicode_version = unicode_version
        end
      end
    end
  end
end
